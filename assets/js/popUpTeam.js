var popUp = {
    init: () => {
        popUp.popTeam('#prof1', '#team-profile-1');
        popUp.popTeam('#prof2', '#team-profile-2');
        popUp.popTeam('#prof3', '#team-profile-3');
        popUp.popTeam('#prof4', '#team-profile-4');
        popUp.popTeam('#prof5', '#team-profile-5');
    },
    popTeam: (sel, sr) => {
        $(sel).magnificPopup({
            items: [{
                src: sr, 
                type: 'inline'
            }],
            /*
            Insert more items to have a gallery
            gallery: {
                enabled: true
              },
            type: 'image'
            */ 
        });

    },
}
$(document).ready(popUp.init);